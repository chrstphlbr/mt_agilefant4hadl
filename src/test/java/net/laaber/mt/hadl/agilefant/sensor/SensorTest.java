package net.laaber.mt.hadl.agilefant.sensor;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorFactory;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.*;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.FactoryResolvingException;
import at.ac.tuwien.dsg.hadl.schema.core.HADLmodel;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;
import at.ac.tuwien.dsg.hadl.surrogates.agilefant.rd.TAgilefantResourceDescriptor;
import com.google.inject.*;
import fj.data.Either;
import fj.data.Option;
import net.laaber.mt.dsl.lib.hadl.*;
import net.laaber.mt.hadl.agilefant.HadlConstants;
import net.laaber.mt.hadl.agilefant.resourceDescriptor.AgilefantResourceDescriptorFactory;
import net.laaber.mt.hadl.atlassian.resourceDescriptor.TAtlassianResourceDescriptor;
import org.junit.*;
import org.junit.rules.TestName;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.test.TestGraphDatabaseFactory;

import java.util.*;

import static org.junit.Assert.assertTrue;

/**
 * Created by Christoph on 03.11.15.
 */
public class SensorTest {
    private final static String MODEL_FILE = "./src/test/resources/input/agile-hADL.xml";
    private final static String OUTPUT_PATH_PREFIX = "src/test/resources/output/";

    private static final int Sprint1 = 156935;
    private static final int Story1 = 714368;
    private static final int Story2 = 714369;
    private static final int Story3 = 714370;

    private ProcessScope ps;

    @Rule
    public TestName testName = new TestName();

    @Before
    public void setUp() {
        final HadlModelInteractor mi = new HadlModelInteractorImpl();
        Either<Throwable, HADLmodel> loadResult = mi.load(MODEL_FILE);
        assertTrue(loadResult.isRight());
        final HADLmodel model = loadResult.right().value();

        final Injector baseInjector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
            }

            @Provides
            @Singleton
            Executable2OperationalTransformer getTransformer() {
                return new Executable2OperationalTransformer();
            }

            @Provides
            @Singleton
            ModelTypesUtil getTypesUtil() {
                ModelTypesUtil mtu = new ModelTypesUtil();
                mtu.init(model);
                return mtu;
            }
        });

        final Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(HADLLinkageConnector.class).in(Singleton.class);
                bind(HADLruntimeMonitor.class).in(Singleton.class);
                bind(HadlLinker.class).to(HadlLinkerImpl.class).in(Singleton.class);
                bind(HadlMonitor.class).to(HadlMonitorImpl.class).in(Singleton.class);
                bind(ProcessScope.class).in(Singleton.class);
                bind(SensorFactory.class).to(AgilefantSensorFactory.class).in(Singleton.class);
            }

            @Provides
            @Singleton
            HadlModelInteractor getModelInteractor() {
                return mi;
            }

            @Provides
            @Singleton
            Executable2OperationalTransformer getTransformer() {
                return baseInjector.getInstance(Executable2OperationalTransformer.class);
            }

            @Provides
            @Singleton
            RuntimeRegistry getRegistry() {
                return new RuntimeRegistry();
            }

            @Provides
            @Singleton
            ModelTypesUtil getTypesUtil() {
                return baseInjector.getInstance(ModelTypesUtil.class);
            }

            @Provides
            @Singleton
            SurrogateFactoryResolver getFactoryResolver() {
                SurrogateFactoryResolver sfr = new SurrogateFactoryResolver();
                try {
                    sfr.resolveFactoriesFrom(model);
                } catch (FactoryResolvingException e) {
                    e.printStackTrace();
                }
                return sfr;
            }

            @Provides
            @Singleton
            HADLruntimeModel getRuntimeModel() {
                Neo4JbackedRuntimeModel hrm = new Neo4JbackedRuntimeModel();
                baseInjector.injectMembers(hrm);
                // setup Neo4J DB
                GraphDatabaseService graphDb = new TestGraphDatabaseFactory().newImpermanentDatabase();
                hrm.init(graphDb, model);
                return hrm;
            }
        });
        ps = injector.getInstance(ProcessScope.class);

        initLinker();
    }

    @Before
    public void printBeginDelimiter() {
        System.out.println("---------- " + testName.getMethodName() + " BEGIN");
    }

    @After
    public void printAfterDelimiter() {
        System.out.println("---------- " + testName.getMethodName() + " END");
    }

    @After
    public void tearDown() {
        storeRt(testName.getMethodName() + ".xml");
        ps.getLinkageConnector().shutdown();
        ps = null;
    }

    private void initLinker() {
        Option<Throwable> setUpError = ps.getLinker().setUp(null);
        assertTrue(setUpError.isNone());
    }

    private void storeRt(String fileName) {
        List<String> e = new ArrayList<>();
        e.add("at.ac.tuwien.dsg.hadl.schema.extension.google");
        Option<Throwable> r = ps.getModelInteractor().storeRuntime(ps.getModel(), ps.getRuntimeModel(), e, OUTPUT_PATH_PREFIX, fileName);
        if (r.isSome()) {
            r.some().printStackTrace();
        }
    }

    private TOperationalObject sprint() {
        TAgilefantResourceDescriptor rd = AgilefantResourceDescriptorFactory.agilefant("Sprint1", Sprint1);
        Either<Throwable, THADLarchElement> sprint = ps.getLinker().acquire(HadlConstants.Obj.Sprint, rd);
        Assert.assertTrue("Returned Error from acquire", sprint.isRight());
        Assert.assertTrue("Not correct Operational", sprint.right().value() instanceof TOperationalObject);
        TOperationalObject s = (TOperationalObject) sprint.right().value();
        return s;
    }

    @Test
    public void loadStoryFromSprint() {
        TOperationalObject s = sprint();

        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>(HadlConstants.Obj.Story, HadlConstants.ObjRef.ContainsStory);
        final Either<Throwable, List<THADLarchElement>> loadResult = ps.getMonitor().load(what, s, null);
        if (loadResult.isLeft()) {
            loadResult.left().value().printStackTrace();
            Assert.fail("Could not load Stories");
        }
        List<THADLarchElement> loadElements = loadResult.right().value();
        Assert.assertTrue("Incorrect element size. Expected 3, got " + loadElements.size(), loadElements.size() == 3);

        for (THADLarchElement e : loadElements) {
            if (e instanceof TOperationalObject) {
                THADLarchElement elType = ps.getModelTypesUtil().getByTypeRef(((TOperationalObject) e).getInstanceOf());
                Assert.assertTrue("Load element of incorrect hADL type: " + elType.getId(), HadlConstants.Obj.Story.equals(elType.getId()));
                for (TResourceDescriptor r : ((TOperationalObject) e).getResourceDescriptor()) {
                    if (r instanceof TAgilefantResourceDescriptor) {
                        System.out.println(((TAgilefantResourceDescriptor) r).getAgilefantid());
                    } else {
                        Assert.fail("Incorrect RD type: " + r.getClass().getSimpleName());
                    }
                }
            } else {
                Assert.fail("Load element of incorrect java type: " + e.getClass().getSimpleName());
            }
        }
    }

    @Test
    public void loadDevUserFromSprint() {
        TOperationalObject s = sprint();

        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>(HadlConstants.Col.DevUser, HadlConstants.ColRef.DevUser);
        List<Map.Entry<String, String>> vias = new ArrayList<>();
        vias.add(new AbstractMap.SimpleImmutableEntry<String, String>(HadlConstants.Obj.Story, HadlConstants.ObjRef.ContainsStory));
        vias.add(new AbstractMap.SimpleImmutableEntry<String, String>(HadlConstants.Col.AgileUser, HadlConstants.Link.ResponsibleStory));

        final Either<Throwable, Set<THADLarchElement>> loadRes = ps.getMonitor().loadWithoutDuplicates(what, s, vias);
        if (loadRes.isLeft()) {
            loadRes.left().value().printStackTrace();
            Assert.fail("Could not load DevUsers");
        }
        Set<THADLarchElement> load = loadRes.right().value();
        Assert.assertEquals("Incorrect element size. Expected 3, got " + load.size(), 3, load.size());

        for (THADLarchElement e : load) {
            if (e instanceof TOperationalComponent) {
                THADLarchElement elType = ps.getModelTypesUtil().getByTypeRef(((TOperationalComponent) e).getInstanceOf());
                Assert.assertTrue("Load element of incorrect hADL type: " + elType.getId(), HadlConstants.Col.DevUser.equals(elType.getId()));
                for (TResourceDescriptor r : ((TOperationalComponent) e).getResourceDescriptor()) {
                    if (r instanceof TAtlassianResourceDescriptor) {
                        System.out.println(((TAtlassianResourceDescriptor) r).getId());
                    } else {
                        Assert.fail("Incorrect RD type: " + r.getClass().getSimpleName());
                    }
                }
            } else {
                Assert.fail("Load element of incorrect java type: " + e.getClass().getSimpleName());
            }
        }
    }
}
