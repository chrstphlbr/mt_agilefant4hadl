package net.laaber.mt.hadl.agilefant.sensor;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.AbstractAsyncSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;
import at.ac.tuwien.dsg.hadl.surrogates.agilefant.RESTendpoint.AgilefantClient;
import at.ac.tuwien.dsg.hadl.surrogates.agilefant.rd.TAgilefantResourceDescriptor;
import rx.subjects.BehaviorSubject;

/**
 * Created by Christoph on 03.11.15.
 */
public abstract class AbstractAgilefantSensor extends AbstractAsyncSensor {

    protected final ModelTypesUtil mtu;
    protected final AgilefantClient client;
    protected TAgilefantResourceDescriptor rd;

    AbstractAgilefantSensor(ModelTypesUtil mtu, AgilefantClient client) {
        this.mtu = mtu;
        this.client = client;
    }

    @Override
    public void asyncAcquire(final TActivityScope tActivityScope, final TOperationalComponent tOperationalComponent, final BehaviorSubject<SensorEvent> behaviorSubject) {
        behaviorSubject.onError(new IllegalStateException(this.getClass().getSimpleName() + " does not support asyncAcquire of TOperationalComponent"));
    }

    @Override
    public void asyncAcquire(final TActivityScope tActivityScope, final TOperationalConnector tOperationalConnector, final BehaviorSubject<SensorEvent> behaviorSubject) {
        behaviorSubject.onError(new IllegalStateException(this.getClass().getSimpleName() + " does not support asyncAcquire of TOperationalConnector"));
    }

    @Override
    public void asyncAcquire(final TActivityScope tActivityScope, final TOperationalObject tOperationalObject, final BehaviorSubject<SensorEvent> behaviorSubject) {
        behaviorSubject.onError(new IllegalStateException(this.getClass().getSimpleName() +  " does not support asyncAcquire of TOperationalObject"));
    }

    protected boolean setRd(Iterable<TResourceDescriptor> rds) {
        for (TResourceDescriptor rd : rds) {
            if (rd instanceof TAgilefantResourceDescriptor) {
                this.rd = (TAgilefantResourceDescriptor) rd;
                return true;
            }
        }
        return false;
    }
}
