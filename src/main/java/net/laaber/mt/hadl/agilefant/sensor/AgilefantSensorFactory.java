package net.laaber.mt.hadl.agilefant.sensor;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollabObjectSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollabSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollaboratorSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorFactory;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.InsufficientModelInformationException;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabObject;
import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.surrogates.agilefant.RESTendpoint.AgilefantClient;
import net.laaber.mt.hadl.agilefant.HadlConstants;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Christoph on 03.11.15.
 */
public class AgilefantSensorFactory implements SensorFactory {

    private static final String credentialsFile = "properties-credentials.xml";

    private final ModelTypesUtil mtu;
    private final AgilefantClient client;

    @Inject
    public AgilefantSensorFactory(ModelTypesUtil mtu) throws IOException {
        this.mtu = mtu;
        Properties p = new Properties();
        p.loadFromXML(getClass().getClassLoader().getResourceAsStream(credentialsFile));
        client = new AgilefantClient(p.getProperty("account"), p.getProperty("admin.login"), p.getProperty("admin.pw"));
    }

    @Override
    public ICollabSensor getInstance(final THADLarchElement thadLarchElement) throws InsufficientModelInformationException {
        if (thadLarchElement instanceof TCollaborator) {
            return getInstance((TCollaborator) thadLarchElement);
        } else if (thadLarchElement instanceof TCollabObject) {
            return getInstance((TCollabObject) thadLarchElement);
        } else {
            return null;
        }
    }

    @Override
    public ICollaboratorSensor getInstance(final TCollaborator tCollaborator) throws InsufficientModelInformationException {
        switch (tCollaborator.getId()) {
            case HadlConstants.Col.AgileUser:
                return new AgileUserSensor(mtu, client);
            default:
                return null;
        }
    }

    @Override
    public ICollabObjectSensor getInstance(final TCollabObject tCollabObject) throws InsufficientModelInformationException {
        switch (tCollabObject.getId()) {
            case HadlConstants.Obj.Sprint:
                return new SprintSensor(mtu, client);
            case HadlConstants.Obj.Story:
                return new StorySensor(mtu, client);
            default:
                return null;
        }
    }
}
