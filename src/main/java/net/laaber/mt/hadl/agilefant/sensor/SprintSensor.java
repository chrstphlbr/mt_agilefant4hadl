package net.laaber.mt.hadl.agilefant.sensor;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.LoadEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.OperativeObjectEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensingScope;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabObject;
import at.ac.tuwien.dsg.hadl.schema.core.TObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.surrogates.agilefant.RESTendpoint.AgilefantClient;
import at.ac.tuwien.dsg.hadl.surrogates.agilefant.pojos.BacklogTO;
import at.ac.tuwien.dsg.hadl.surrogates.agilefant.pojos.StoryTO;
import at.ac.tuwien.dsg.hadl.surrogates.agilefant.rd.TAgilefantResourceDescriptor;
import com.google.api.client.http.HttpResponseException;
import net.laaber.mt.hadl.agilefant.HadlConstants;
import net.laaber.mt.hadl.agilefant.resourceDescriptor.AgilefantResourceDescriptorFactory;
import rx.subjects.BehaviorSubject;

/**
 * Created by Christoph on 03.11.15.
 */
public class SprintSensor extends AbstractAgilefantSensor {

    private BacklogTO sprint;

    SprintSensor(ModelTypesUtil mtu, AgilefantClient client) {
        super(mtu, client);
    }

    @Override
    public void asyncAcquire(final TActivityScope tActivityScope, final TOperationalObject tOperationalObject, final BehaviorSubject<SensorEvent> behaviorSubject) {
        behaviorSubject.onNext(new SensorEvent(SensorEvent.ESensorStatus.ACQUIRING_INPROGRESS, this));
        if (!setRd(tOperationalObject.getResourceDescriptor())) {
            behaviorSubject.onError(new IllegalStateException("No suitable ResourceDescriptor found"));
            return;
        }

        try {
            sprint = client.getBacklogTreeById(rd.getAgilefantid());
            behaviorSubject.onNext(new SensorEvent(SensorEvent.ESensorStatus.ACQUIRING_SUCCESS, this));
        } catch (HttpResponseException e) {
            behaviorSubject.onNext(new SensorEvent(SensorEvent.ESensorStatus.ACQUIRING_FAILED, e, this));
        }

        behaviorSubject.onCompleted();
    }

    @Override
    public void asyncLoadOnce(final BehaviorSubject<LoadEvent> behaviorSubject, final SensingScope sensingScope) {
        for (TObjectRef r : sensingScope.getObjectRefs()) {
            switch (r.getId()) {
                case HadlConstants.ObjRef.ContainsStory:
                    loadStories(behaviorSubject, sensingScope, r);
                    break;
                default:
                    break;
            }
        }
        behaviorSubject.onCompleted();
    }

    private void loadStories(final BehaviorSubject<LoadEvent> s, final SensingScope scope, final TObjectRef r) {
        try {
            final StoryTO.StoryList stories = client.getBacklogStoriesByBacklogId(sprint.getId());
            for (StoryTO story : stories) {
                TAgilefantResourceDescriptor rd = AgilefantResourceDescriptorFactory.agilefant(story.getName(), story.getId());
                OperativeObjectEvent e = new OperativeObjectEvent((TCollabObject) mtu.getById(HadlConstants.Obj.Story), this.oc, scope);
                e.addDescriptor(rd);
                e.addLoadedViaRef(r);
                s.onNext(e);
            }
        } catch (HttpResponseException e) {
            e.printStackTrace();
        }
    }
}
