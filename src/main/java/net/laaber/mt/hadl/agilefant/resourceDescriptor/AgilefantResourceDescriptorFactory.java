package net.laaber.mt.hadl.agilefant.resourceDescriptor;

import at.ac.tuwien.dsg.hadl.surrogates.agilefant.rd.TAgilefantResourceDescriptor;
import net.laaber.mt.hadl.agilefant.sensor.AgilefantSensorFactory;

/**
 * Created by Christoph on 03.11.15.
 */
public final class AgilefantResourceDescriptorFactory {

    public AgilefantResourceDescriptorFactory() {}

    public static TAgilefantResourceDescriptor agilefant(String name, Integer agilefantId) {
        TAgilefantResourceDescriptor rd = new TAgilefantResourceDescriptor();
        rd.setId("agilefantRd:" + name + ":" + agilefantId);
        rd.setName(name);
        rd.setAgilefantid(agilefantId);
        return rd;
    }

}
