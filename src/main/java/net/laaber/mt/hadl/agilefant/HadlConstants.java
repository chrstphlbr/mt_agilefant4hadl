package net.laaber.mt.hadl.agilefant;

/**
 * Created by Christoph on 29.10.15.
 */
public final class HadlConstants {
    private HadlConstants() {}

    private static final String Scrum = "scrum.";

    private static final String StructureObj = Scrum + "obj.";
    private static final String StructureCol = Scrum + "col.";
    private static final String StructureLinks = Scrum + "links.";

    public static final class Obj {
        public static final String Sprint = StructureObj + "Sprint";
        public static final String Story = StructureObj + "Story";
        public static final String Wiki = StructureObj + "Wiki";
        public static final String Chat = StructureObj + "Chat";
    }

    public static final class Col {
        public static final String DevUser = StructureCol + "DevUser";
        public static final String AgileUser = StructureCol + "AgileUser";
    }

    public static final class ColRef {
        public static final String DevUser = StructureCol + "devUser";
        public static final String AgileUser = StructureCol + "agileUser";
    }

    public static final class ObjRef {
        public static final String ContainsStory = StructureObj + "containsStory";
        public static final String PrevWiki = StructureObj + "prev";
        public static final String NextWiki = StructureObj + "next";
    }

    public static final class Link {
        public static final String ResponsibleStory = StructureLinks + "responsible";
        public static final String InviteChat = StructureLinks + "invite";
    }
}
