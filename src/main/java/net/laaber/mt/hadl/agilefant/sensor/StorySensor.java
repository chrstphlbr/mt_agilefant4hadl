package net.laaber.mt.hadl.agilefant.sensor;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.LoadEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.OperativeCollaboratorEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensingScope;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabLink;
import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.surrogates.agilefant.RESTendpoint.AgilefantClient;
import at.ac.tuwien.dsg.hadl.surrogates.agilefant.pojos.StoryTO;
import at.ac.tuwien.dsg.hadl.surrogates.agilefant.pojos.UserTO;
import at.ac.tuwien.dsg.hadl.surrogates.agilefant.rd.TAgilefantResourceDescriptor;
import com.google.api.client.http.HttpResponseException;
import net.laaber.mt.hadl.agilefant.HadlConstants;
import net.laaber.mt.hadl.agilefant.resourceDescriptor.AgilefantResourceDescriptorFactory;
import rx.subjects.BehaviorSubject;

/**
 * Created by Christoph on 03.11.15.
 */
public class StorySensor extends AbstractAgilefantSensor {

    private StoryTO story;

    StorySensor(ModelTypesUtil mtu, AgilefantClient client) {
        super(mtu, client);
    }

    @Override
    public void asyncAcquire(final TActivityScope tActivityScope, final TOperationalObject tOperationalObject, final BehaviorSubject<SensorEvent> behaviorSubject) {
        behaviorSubject.onNext(new SensorEvent(SensorEvent.ESensorStatus.ACQUIRING_INPROGRESS, this));
        if (!setRd(tOperationalObject.getResourceDescriptor())) {
            behaviorSubject.onError(new IllegalStateException("No suitable ResourceDescriptor found"));
            return;
        }

        try {
            story = client.getStoryById(rd.getAgilefantid());
            behaviorSubject.onNext(new SensorEvent(SensorEvent.ESensorStatus.ACQUIRING_SUCCESS, this));
        } catch (HttpResponseException e) {
            behaviorSubject.onNext(new SensorEvent(SensorEvent.ESensorStatus.ACQUIRING_FAILED, e, this));
        }

        behaviorSubject.onCompleted();
    }

    @Override
    public void asyncLoadOnce(final BehaviorSubject<LoadEvent> behaviorSubject, final SensingScope sensingScope) {
        for (TCollabLink l : sensingScope.getLinks()) {
            switch (l.getId()) {
                case HadlConstants.Link.ResponsibleStory:
                    loadUsers(behaviorSubject, sensingScope, l);
                    break;
                default:
                    break;
            }
        }
        behaviorSubject.onCompleted();
    }

    private void loadUsers(final BehaviorSubject<LoadEvent> s, final SensingScope scope, final TCollabLink l) {
        for (UserTO u : story.getResponsibles()) {
            try {
                UserTO loadedUser = client.getUserById(u.getId());
                TAgilefantResourceDescriptor rd = AgilefantResourceDescriptorFactory.agilefant(loadedUser.getEmail(), loadedUser.getId());
                OperativeCollaboratorEvent e = new OperativeCollaboratorEvent((TCollaborator) mtu.getById(HadlConstants.Col.AgileUser), this.oc, scope);
                e.addLoadedViaLinkType(l);
                e.addDescriptor(rd);
                s.onNext(e);
            } catch (HttpResponseException e) {
                e.printStackTrace();
            }
        }
    }
}
