package net.laaber.mt.hadl.agilefant.sensor;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.LoadEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.OperativeCollaboratorEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensingScope;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorEvent;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabRef;
import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.surrogates.agilefant.RESTendpoint.AgilefantClient;
import at.ac.tuwien.dsg.hadl.surrogates.agilefant.pojos.UserTO;
import com.google.api.client.http.HttpResponseException;
import fj.data.Option;
import net.laaber.mt.hadl.agilefant.HadlConstants;
import net.laaber.mt.hadl.atlassian.data.hipChat.User;
import net.laaber.mt.hadl.atlassian.resourceDescriptor.AtlassianResourceDescriptorFactory;
import net.laaber.mt.hadl.atlassian.resourceDescriptor.TAtlassianResourceDescriptor;
import net.laaber.mt.hadl.atlassian.rest.AtlassianClientFactory;
import net.laaber.mt.hadl.atlassian.rest.HipChatClient;
import rx.subjects.BehaviorSubject;

import java.io.IOException;

/**
 * Created by Christoph on 03.11.15.
 */
public class AgileUserSensor extends AbstractAgilefantSensor {

    private UserTO user;
    private HipChatClient hcClient;

    AgileUserSensor(ModelTypesUtil mtu, AgilefantClient client) {
        super(mtu, client);
    }

    private HipChatClient hcClient() {
        if (hcClient == null) {
            synchronized (this) {
                if (hcClient == null) {
                    try {
                        hcClient = AtlassianClientFactory.instance().hipChatClient();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return hcClient;
    }

    @Override
    public void asyncAcquire(final TActivityScope tActivityScope, final TOperationalComponent tOperationalComponent, final BehaviorSubject<SensorEvent> behaviorSubject) {
        behaviorSubject.onNext(new SensorEvent(SensorEvent.ESensorStatus.ACQUIRING_INPROGRESS, this));
        if (!setRd(tOperationalComponent.getResourceDescriptor())) {
            behaviorSubject.onError(new IllegalStateException("No suitable ResourceDescriptor found"));
            return;
        }

        try {
            user = client.getUserById(rd.getAgilefantid());
            behaviorSubject.onNext(new SensorEvent(SensorEvent.ESensorStatus.ACQUIRING_SUCCESS, this));
        } catch (HttpResponseException e) {
            behaviorSubject.onNext(new SensorEvent(SensorEvent.ESensorStatus.ACQUIRING_FAILED, e, this));
        }
        behaviorSubject.onCompleted();
    }

    @Override
    public void asyncLoadOnce(final BehaviorSubject<LoadEvent> behaviorSubject, final SensingScope sensingScope) {
        for (TCollabRef r : sensingScope.getCollabRefs()) {
            switch (r.getId()) {
                case HadlConstants.ColRef.DevUser:
                    loadDevUser(behaviorSubject, sensingScope, r);
                    break;
                default:
                    break;
            }
        }
        behaviorSubject.onCompleted();
    }

    private void loadDevUser(final BehaviorSubject<LoadEvent> s, final SensingScope scope, final TCollabRef r) {
        Option<User> user = hcClient().user(rd.getName());
        if (user.isSome()) {
            String email = user.some().getEmail();
            TAtlassianResourceDescriptor rd = AtlassianResourceDescriptorFactory.atlassian(email, email, email);
            OperativeCollaboratorEvent e = new OperativeCollaboratorEvent((TCollaborator) mtu.getById(HadlConstants.Col.DevUser), this.oc1, scope);
            e.addLoadedViaRef(r);
            e.addDescriptor(rd);
            s.onNext(e);
        }
    }
}
